import $ from 'jquery';
import 'slick-carousel';
const WheelIndicator = require('wheel-indicator');

export default {
  init() {
    // JavaScript to be fired on the home page

    // Home page carosuel
    let scrolling;
    const $carousel = $('.js-home-slider');
    const gradSlideOne = $('.home-page-slide--one');
    const gradSlideTwo = $('.home-page-slide--two');
    const gradSlideThree = $('.home-page-slide--three');

    $carousel.on('init', function () {
      new WheelIndicator({
        elem: $carousel[0],
        callback: wheelHandler,
      });
    }).on('beforeChange', function () {
      scrolling = true;
    }).on('afterChange', function () {
      scrolling = false;
    }).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: false,
      speed: 900,
      fade: true,
      cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
      dots: true,
      customPaging: function(slider, i) { 
        return '<button class="tab"><small class="tab-about d-none d-lg-block tab-about--'+ i +'">' + $(slider.$slides[i]).children().children().attr('title') + '</small><div class="font-weight-light h2 ml-lg-3 mb-0">0' + (i + 1) +'</div></button>';
      },
    });

    // Home page carousel wheel handler
    function wheelHandler(e) {
      if (!scrolling) {
        $carousel.slick(e.direction === 'up' ? 'slickPrev' : 'slickNext');
  
        if (gradSlideOne.parents().parents().hasClass('slick-active')) {
          console.log('slide 1');
        }
  
        if (gradSlideTwo.parents().parents().hasClass('slick-active')) {
          console.log('slide 2');
        }
  
        if (gradSlideThree.parents().parents().hasClass('slick-active')) {
           console.log('slide 3');
        }
      }
    }

    // Home Ppage menu
    const menuToggler = document.getElementById('menuToggler')
    const menuTogglerLabel = document.getElementById('menuTogglerLabel');
    const sidebar = document.getElementById('sidebar');

    menuToggler.addEventListener('change', function() {
      if(menuToggler.checked) {
        menuTogglerLabel.setAttribute('aria-pressed', 'true');
        sidebar.setAttribute('aria-hidden', 'false');    
      } else {
        menuTogglerLabel.setAttribute('aria-pressed', 'false');
        sidebar.setAttribute('aria-hidden', 'true');
      }
    });

    const icnMenu = document.querySelector('.menu-icon');
    icnMenu.addEventListener('click', () => {
      icnMenu.classList.toggle('active');
    });
  },

  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
