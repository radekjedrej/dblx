<div class="left-bar d-none d-xl-block">
  <div class="left-bar__inner py-4 d-flex flex-column justify-content-end align-items-center h-100">

    <div class="vertical-text scroll-navigate text-right">
      <small><span class="hr-before">SCROLL</span><br>TO NAVIGATE</small>
    </div>
  </div>
</div>