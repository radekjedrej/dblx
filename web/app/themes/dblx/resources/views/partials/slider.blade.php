<div class="js-home-slider mb-0">
  <div title="ABOUT<br>MCG">
    <div class="home-page-slide home-page-slide--one" style="background-image:url(@asset('images/background/mcg-landing-two.png'))">

      <div class="container h-100 position-relative">
        <div class="d-flex align-items-center h-100">
          <div class="progress-slider__wrapper">
            <div class="progress-slider d-flex align-items-center mb-2">
              <small class="mr-2">01</small>
              <div class="progress flex-grow-1">
                <div class="progress-bar" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <small class="ml-2">02</small>
              <small class="ml-3">Welcome</small>
            </div>
            <h2 class="h1">Creating Human<br>engagement.</h2>
            <p class="h3 font-weight-light">through technology.</p>
            <div class="animated-arrow__wrapper">
              <a class="animated-arrow" href="https://google.com">
                <span class="the-arrow -left">
                  <span class="shaft"></span>
                </span>
                <span class="arrow-main">
                  <span class="text">
                    DISCOVER MCG
                  </span>
                  <span class="the-arrow -right">
                    <span class="shaft"></span>
                  </span>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div class="row text-container justify-content-center h-100">
          <div class="col-14 h-100 background-line-wide">
            <span class="text-container__word text-container__top display-1">Human</span>
            <span class="text-container__word text-container__bottom display-1">Engagment</span>
          </div>
        </div>

      </div>

    </div>
  </div>

  <div title="OUR<br>BRANDS">
    <div class="home-page-slide home-page-slide--two" style="background-image:url(@asset('images/background/mcg-landing-two.png'))"> 

      <div class="container h-100 position-relative">
        <div class="d-flex align-items-center h-100">
          <div class="progress-slider__wrapper">
            <div class="progress-slider d-flex align-items-center mb-2">
              <small class="mr-2">02</small>
              <div class="progress flex-grow-1">
                <div class="progress-bar" role="progressbar" style="width: 66%" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <small class="ml-2">03</small>
              <small class="ml-3">Welcome</small>
            </div>
            <h2 class="h1">Creating Human<br>engagement.</h2>
            <p class="h3 font-weight-light">through technology.</p>
            <div class="animated-arrow__wrapper">
              <a class="animated-arrow" href="https://google.com">
                <span class="the-arrow -left">
                  <span class="shaft"></span>
                </span>
                <span class="arrow-main">
                  <span class="text">
                    DISCOVER MCG
                  </span>
                  <span class="the-arrow -right">
                    <span class="shaft"></span>
                  </span>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div class="row text-container justify-content-center h-100">
          <div class="col-14 h-100 background-line-wide">
            <span class="text-container__word text-container__top display-1">Human</span>
            <span class="text-container__word text-container__bottom display-1">Engagment</span>
          </div>
        </div>

      </div>
      
    </div>
  </div>

<div title="EXPLORE<br>CAREERS">
  <div class="home-page-slide home-page-slide--three" style="background-image:url(@asset('images/background/mcg-landing-two.png'))">

    <div class="container h-100 position-relative">
      <div class="d-flex align-items-center h-100">
        <div class="progress-slider__wrapper">
          <div class="progress-slider d-flex align-items-center mb-2">
            <small class="mr-2">03</small>
            <div class="progress flex-grow-1">
              <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <small class="ml-2">01</small>
            <small class="ml-3">Welcome</small>
          </div>
          <h2 class="h1">Creating Human<br>engagement.</h2>
          <p class="h3 font-weight-light">through technology.</p>
          <div class="animated-arrow__wrapper">
            <a class="animated-arrow" href="https://google.com">
              <span class="the-arrow -left">
                <span class="shaft"></span>
              </span>
              <span class="arrow-main">
                <span class="text">
                  DISCOVER MCG
                </span>
                <span class="the-arrow -right">
                  <span class="shaft"></span>
                </span>
              </span>
            </a>
          </div>
        </div>
      </div>

      <div class="row text-container justify-content-center h-100">
        <div class="col-14 h-100 background-line-wide">
          <span class="text-container__word text-container__top display-1">Human</span>
          <span class="text-container__word text-container__bottom display-1">Engagment</span>
        </div>
      </div>
      
    </div>

  </div>
</div>


</div>